import "@babylonjs/core/Debug/debugLayer";
import "@babylonjs/inspector";
import "@babylonjs/loaders/glTF";
import { Engine, Scene, ArcRotateCamera, Vector3, HemisphericLight, Mesh, MeshBuilder, SceneLoader, Camera, UniversalCamera, Observable, SpotLight, GizmoManager, LightGizmo, Light, StandardMaterial, Texture, Color3, Color4 } from "@babylonjs/core";

class App {
    private engine:Engine;
    private canvas:HTMLCanvasElement;

    constructor() {
        // create the canvas html element and attach it to the webpage
        this.canvas = document.createElement("canvas");
        this.canvas.style.width = "100%";
        this.canvas.style.height = "100%";
        this.canvas.id = "gameCanvas";
        document.body.appendChild(this.canvas);

        // initialize babylon scene and engine
        this.engine = new Engine(this.canvas, true);
        this.accueil();
    }

    /* Des poissons qui nagent et un menu */
    accueil() {
        var scene = new Scene(this.engine);
        scene.clearColor = new Color4(0, 0, 0, 1);
        var camera: ArcRotateCamera = new ArcRotateCamera("Camera", Math.PI / 2, Math.PI / 4, 20, Vector3.Zero(), scene);
        const box = MeshBuilder.CreateBox("box", {height: 1, width: 1, depth: 1}, scene);
        box.position = new Vector3(5, 0, 0);
        //box.visibility = 0;
        camera.fov = 0.5;
        var gizmoManager = new GizmoManager(scene);
    
        gizmoManager.positionGizmoEnabled = true;
        gizmoManager.rotationGizmoEnabled = true;
        gizmoManager.scaleGizmoEnabled = true;
        gizmoManager.boundingBoxGizmoEnabled = true;

        const spotLight = new SpotLight(
            "spotLight",
            new Vector3(0, 10, 0),
            new Vector3(0, -1, 0),
            Math.PI / 2,
            100,
            scene
          );
      
          spotLight.intensity = 100;
      
          spotLight.shadowEnabled = true;
          spotLight.shadowMinZ = 1;
          spotLight.shadowMaxZ = 10;
        createGizmos(spotLight, scene);
        SceneLoader.ImportMeshAsync("", "models/", "bassinIntro.glb", scene).then((result) => {
            result.meshes[0].position._z = result.meshes[0].position._z+10000;
            result.meshes[0].position = new Vector3(0, 1.5, 0);
        });
        const ground = MeshBuilder.CreateGround("ground", {width:20, height:20}, scene);
        const groundMat = new StandardMaterial("roofMat", scene);
        var texture:Texture = new Texture("models/graviers.png", scene);
        texture.uScale = 6;
        texture.vScale = 6;
        groundMat.diffuseTexture = texture;
        groundMat.specularPower = 100;
        ground.material = groundMat;
        camera.attachControl(this.canvas, true);
        this.runRenderLoop(scene);
        camera.parent = box;
        camera.onAfterCheckInputsObservable.add(function(){

        
                //console.log("camera pos=", camera.position);
                //console.log("camera rot=", camera.rotationQuaternion);
        
            })
        console.log(camera.globalPosition);
    }

    runRenderLoop(scene:Scene) {
        this.engine.runRenderLoop(() => {
            scene.render();
        });
    }

}
new App();

window.onresize = function (event) {
    redimensionnement();
}

window.onload = function (event) {
    redimensionnement();
}

function redimensionnement(){
    console.log(document.getElementById('gameCanvas'));
    let canvas: HTMLCanvasElement = <HTMLCanvasElement> document.getElementById('gameCanvas');
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
}

function createGizmos(customLight: Light, scene:Scene): void {
    const lightGizmo = new LightGizmo();
    lightGizmo.scaleRatio = 2;
    lightGizmo.light = customLight;

    const gizmoManager = new GizmoManager(scene);
    gizmoManager.positionGizmoEnabled = true;
    gizmoManager.rotationGizmoEnabled = true;
    gizmoManager.usePointerToAttachGizmos = false;
    gizmoManager.attachToMesh(lightGizmo.attachedMesh);
  }